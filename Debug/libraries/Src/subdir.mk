################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../libraries/Src/disc1_library.c 

OBJS += \
./libraries/Src/disc1_library.o 

C_DEPS += \
./libraries/Src/disc1_library.d 


# Each subdirectory must supply rules for building sources it contributes
libraries/Src/%.o libraries/Src/%.su libraries/Src/%.cyclo: ../libraries/Src/%.c libraries/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../USB_DEVICE/App -I../USB_DEVICE/Target -I../Middlewares/ST/STM32_USB_Device_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -I"C:/Users/becer/STM32CubeIDE/workspace_1.12.0/FW_Project/libraries/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-libraries-2f-Src

clean-libraries-2f-Src:
	-$(RM) ./libraries/Src/disc1_library.cyclo ./libraries/Src/disc1_library.d ./libraries/Src/disc1_library.o ./libraries/Src/disc1_library.su

.PHONY: clean-libraries-2f-Src

