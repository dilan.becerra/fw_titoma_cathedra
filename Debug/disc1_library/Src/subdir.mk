################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../disc1_library/Src/disc1_library.c 

OBJS += \
./disc1_library/Src/disc1_library.o 

C_DEPS += \
./disc1_library/Src/disc1_library.d 


# Each subdirectory must supply rules for building sources it contributes
disc1_library/Src/%.o disc1_library/Src/%.su disc1_library/Src/%.cyclo: ../disc1_library/Src/%.c disc1_library/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F429xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -I../USB_DEVICE/App -I../USB_DEVICE/Target -I../Middlewares/ST/STM32_USB_Device_Library/Core/Inc -I../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc -I"C:/Users/becer/STM32CubeIDE/workspace_1.12.0/FW_Project/disc1_library/Inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-disc1_library-2f-Src

clean-disc1_library-2f-Src:
	-$(RM) ./disc1_library/Src/disc1_library.cyclo ./disc1_library/Src/disc1_library.d ./disc1_library/Src/disc1_library.o ./disc1_library/Src/disc1_library.su

.PHONY: clean-disc1_library-2f-Src

