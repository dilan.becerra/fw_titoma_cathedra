/**
 ****************************************************************************************************************
 * @file 	disc1_library.h
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the input/output definitions and control functions for some DIS1 board accessories
 * @version 1.0.0
 * @date 	May 18, 2023
 ****************************************************************************************************************
 */
/** Define to prevent recursive inclusions --------------------------------------------------------------------*/
#ifndef INC_DISC1_LIBRARY_H_
#define INC_DISC1_LIBRARY_H_

/** Includes --------------------------------------------------------------------------------------------------*/
#include "stdio.h"
#include "stm32f4xx_hal.h"

/** Exported defines ------------------------------------------------------------------------------------------*/
#define DISC1_LED_ANIMATION_INPUT_BUTTON_PIN GPIO_PIN_0
#define DISC1_LED_ANIMATION_INPUT_BUTTON_PORT GPIOA
#define DISC1_LED_ANIMATION_OUTPUT_LED_PIN GPIO_PIN_13
#define DISC1_LED_ANIMATION_OUTPUT_LED_PORT GPIOG

/** Exported types --------------------------------------------------------------------------------------------*/
/** Exported constants ----------------------------------------------------------------------------------------*/
/** Exported macro --------------------------------------------------------------------------------------------*/
/** Exported functions ----------------------------------------------------------------------------------------*/
void DISC1_LEDAnimation_InputOutputInit(void);
void DISC1_LEDAnimation_InThreadHandler(void);

#endif /* INC_DISC1_LIBRARY_H_ */
/************************************************* END OF FILE *************************************************/
