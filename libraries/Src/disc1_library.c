/**
 ****************************************************************************************************************
 * @file 	disc1_library.h
 * @author 	Dilan Becerra (dilan.becerra@utp.edu.co)
 * @brief 	This file contains the input/output definitions and control functions for some DIS1 board accessories
 * @version 1.0.0
 * @date 	May 18, 2023
 ****************************************************************************************************************
 */

/** Includes --------------------------------------------------------------------------------------------------*/
#include "disc1_library.h"

/** Private defines -------------------------------------------------------------------------------------------*/
/** Private typedef -------------------------------------------------------------------------------------------*/
/** Private macro ---------------------------------------------------------------------------------------------*/
/** Private function prototypes -------------------------------------------------------------------------------*/
/** Private variable declaration ------------------------------------------------------------------------------*/
int priv_time_togle=0, priv_counter_to_togle=0;

/** Function definition ---------------------------------------------------------------------------------------*/

/**
 * @brief 		This function initializes the peripherals needed for the LED animation
 * @param		None
 * @retval 		None
 */
void DISC1_LEDAnimation_InputOutputInit(void){
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOG, DISC1_LED_ANIMATION_OUTPUT_LED_PIN, GPIO_PIN_RESET);

	/*Configure GPIO pin : BUTTON_Pin */
	GPIO_InitStruct.Pin = DISC1_LED_ANIMATION_INPUT_BUTTON_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(DISC1_LED_ANIMATION_INPUT_BUTTON_PORT, &GPIO_InitStruct);

	/*Configure GPIO pin : LED_1_Pin */
	GPIO_InitStruct.Pin = DISC1_LED_ANIMATION_OUTPUT_LED_PIN;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(DISC1_LED_ANIMATION_OUTPUT_LED_PORT, &GPIO_InitStruct);
}

/**
 * @brief 		This function handle the toggle of the output LED
 * @param		None
 * @retval 		None
 */
void DISC1_LEDAnimation_InThreadHandler(void){
	if(HAL_GPIO_ReadPin(DISC1_LED_ANIMATION_INPUT_BUTTON_PORT, DISC1_LED_ANIMATION_INPUT_BUTTON_PIN)){
		while(HAL_GPIO_ReadPin(DISC1_LED_ANIMATION_INPUT_BUTTON_PORT, DISC1_LED_ANIMATION_INPUT_BUTTON_PIN));
		if(priv_time_togle==0){
			priv_time_togle=200;
		}else{
			priv_time_togle=priv_time_togle/2;
		}
		if(priv_time_togle<50){
			priv_time_togle=0;
			HAL_GPIO_WritePin(DISC1_LED_ANIMATION_OUTPUT_LED_PORT, DISC1_LED_ANIMATION_OUTPUT_LED_PIN, 0);
		}
		priv_counter_to_togle=0;
	}
	if(priv_time_togle>0){
		HAL_Delay(1);
		priv_counter_to_togle++;
		if(priv_counter_to_togle>priv_time_togle){
			HAL_GPIO_TogglePin(DISC1_LED_ANIMATION_OUTPUT_LED_PORT, DISC1_LED_ANIMATION_OUTPUT_LED_PIN);
			priv_counter_to_togle=0;
		}
	}
}

